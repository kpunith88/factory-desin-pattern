package com.example.pizza;

/**
 * Created by Punith, K on 5/5/2016.
 */
public class NYCheesePizza implements Pizza {
    public void prepare() {
        System.out.println("Preparing the NY Cheese Pizza");
    }

    public void bake() {
        System.out.println("Baking the NY Cheese Pizza");
    }

    public void cut() {
        System.out.println("Cutting the NY Cheese Pizza");
    }

    public void pack() {
        System.out.println("Packing the NY Cheese Pizza");
    }
}
