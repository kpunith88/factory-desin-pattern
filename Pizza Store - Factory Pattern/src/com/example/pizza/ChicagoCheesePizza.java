package com.example.pizza;

/**
 * Created by Punith, K on 5/29/2016.
 */
public class ChicagoCheesePizza implements Pizza {

    public void prepare() {
        System.out.println("Preparing the Chicago Cheese Pizza");
    }

    public void bake() {
        System.out.println("Baking the Chicago Cheese Pizza");
    }

    public void cut() {
        System.out.println("Cutting the Chicago Cheese Pizza");
    }

    public void pack() {
        System.out.println("Packing the Chicago Cheese Pizza");
    }
}
