package com.example.pizza;

/**
 * Created by Punith, K on 5/5/2016.
 */
public abstract class PizzaStore {

    public Pizza orderPizza(String type) {
        Pizza pizza;

        pizza = createPizza(type);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.pack();

        return pizza;
    }

    /**
    Factory method that takes care of creating objects of type Pizza
    */
    protected abstract Pizza createPizza(String type);
}
