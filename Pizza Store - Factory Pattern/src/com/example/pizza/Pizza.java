package com.example.pizza;

/**
 * Created by Punith, K on 5/5/2016.
 */
public interface Pizza {

    void prepare();

    void bake();

    void cut();

    void pack();
}
