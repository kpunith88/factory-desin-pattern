package com.example.pizza;

/**
 * Created by Punith, K on 5/29/2016.
 */
public class ChicagoClamPizza implements Pizza {
    public void prepare() {
        System.out.println("Preparing the Chicago Clam Pizza");
    }

    public void bake() {
        System.out.println("Baking the Chicago Clam Pizza");
    }

    public void cut() {
        System.out.println("Cutting the Chicago Clam Pizza");
    }

    public void pack() {
        System.out.println("Packing the Chicago Clam Pizza");
    }
}
