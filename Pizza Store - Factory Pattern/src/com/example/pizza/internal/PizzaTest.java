package com.example.pizza.internal;

import com.example.pizza.Pizza;
import com.example.pizza.PizzaStore;

/**
 * Created by Punith, K on 5/5/2016.
 */
public class PizzaTest {
    public static void main(String[] args) {

        PizzaStore nyStore = new NYPizzaStore();
        nyStore.orderPizza("cheese");

        System.out.println("-----------------------------------");
        nyStore.orderPizza("clam");

        System.out.println("-----------------------------------");
        PizzaStore chicagoStore = new ChicagoPizzaStore();
        chicagoStore.orderPizza("clam");

        System.out.println("-----------------------------------");
        nyStore.orderPizza("cheese");

    }
}
