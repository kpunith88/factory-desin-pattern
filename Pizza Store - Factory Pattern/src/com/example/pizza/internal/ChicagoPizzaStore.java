package com.example.pizza.internal;

import com.example.pizza.ChicagoCheesePizza;
import com.example.pizza.ChicagoClamPizza;
import com.example.pizza.Pizza;
import com.example.pizza.PizzaStore;

/**
 * Created by Punith, K on 5/29/2016.
 */
public class ChicagoPizzaStore extends PizzaStore {

    @Override
    protected Pizza createPizza(String type) {

        if(type.equals("clam")){
            return new ChicagoClamPizza();
        }
        else if(type.equals("cheese")){
            return new ChicagoCheesePizza();
        } else return null;
    }
}
