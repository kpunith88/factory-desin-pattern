package com.example.pizza.internal;

import com.example.pizza.NYClamPizza;
import com.example.pizza.NYCheesePizza;
import com.example.pizza.Pizza;
import com.example.pizza.PizzaStore;

/**
 * Created by Punith, K on 5/6/2016.
 */
public class NYPizzaStore extends PizzaStore {

    @Override
    protected Pizza createPizza(java.lang.String type) {
        if (type.equals("cheese")) {
            return new NYCheesePizza();
        } else if (type.equals("clam")) {
            return new NYClamPizza();
        } else return null;
    }
}
